ruby-serialport (1.3.2-1) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.
  * Update watch file format version to 4.
  * Bump debhelper from old 12 to 13.

  [ Lucas Kanashiro ]
  * d/watch: use gemwatch.debian.net to track new upstream versions
  * New upstream version 1.3.2 (Closes: #996384)
  * Declare compliance with Debian Policy 4.6.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Wed, 27 Oct 2021 16:19:44 -0300

ruby-serialport (1.3.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - fixes FTBFS with Ruby 2.2 (Closes: #791784)

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 18 Jul 2015 09:35:36 -0300

ruby-serialport (1.3.0-1) unstable; urgency=low

  * Team upload.

  [ Cédric Boutillier ]
  * debian/control
   - remove obsolete DM-Upload-Allowed flag
   - use canonical URI in Vcs-* fields

  [ Jonas Genannt ]
  * Imported Upstream version 1.3.0
  * debian/ruby-tests.rb: added basic testing
  * d/control
    - bumped standards version to 3.9.5 (no changes needed)
    - removed transitional packages
    - updated long description

 -- Jonas Genannt <jonas.genannt@capi2name.de>  Wed, 15 Jan 2014 23:41:33 +0100

ruby-serialport (1.1.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bumped standards version to 3.9.3.
    - Bumped build-depend on gem2deb to 0.3.0.
  * debian/copyright: use Debian copyright format version 1.0.
  * debian/source/lintian-overrides: added to override warnings about the
    descriptions of the transitional packages.
  * debian/rules, debian/ruby-test-files.yaml: stuff under test/ are not
    real test files/suites, removed.

 -- Paul van Tilburg <paulvt@debian.org>  Sun, 24 Jun 2012 17:11:03 +0200

ruby-serialport (1.0.4-1) unstable; urgency=low

  * New upstream release (closes: #597526).
  * Source packages adapted according to the new Ruby policy:
    - Build for both ruby1.8 and ruby1.9.1.
    - Migrated to pkg-ruby-extras git repos. Changed the Vcs-* fields in
      debian/control accordingly.
    - Changed the depends and recommends to follow the new Ruby
      library naming scheme.
  * debian/control:
    - Added a default DM-Upload-Allowed field set to yes.
    - Standards-Version bumped to 3.9.2; no changes required.
    - Set XS-Ruby-Versions to all.
    - Changed the build-depends for using gem2deb instead of ruby-pkg-tools.
    - Switched the maintainer with the uploaders field as per new
      convention the team is the default maintainer.
    - Added libserialport-ruby and libserialport-ruby1.8 as transitional
      packages.
  * debian/copyright: reworked to fit the DEP5 format.
  * debian/rules: disable tests for now, a TTY device is not guaranteed to be
    available.

 -- Paul van Tilburg <paulvt@debian.org>  Wed, 28 Dec 2011 16:27:04 +0100

libserialport-ruby (0.7.0-1) unstable; urgency=low

  [ Arnaud Cornet ]
  * Update my mail address.

  [ Gunnar Wolf ]
  * Changed section to Ruby as per ftp-masters' request

  [ Paul van Tilburg ]
  * New upstream release.
  * debian/control:
    - Bumped standards version to 3.8.1.
    - Added depend on ${misc:Depends} for libserialport-ruby.
    - Added build-depend on libsetup-ruby1.8.
  * debian/rules:
    - Switched from upstream extconf to Debian's native setup.rb-based
      building.
    - Also load the CDBS simple-patchsys class.
  * Added debian/libserialport-ruby.examples.
  * Added debian/patches/01-add-shebang-fix-load.patch to fix the example
    so that it complies with policy and loads the correct lib.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 05 May 2009 14:42:33 +0200

libserialport-ruby (0.6-3) unstable; urgency=low

  * Build-depend on r-p-t >= 0.14, to fix installation of ruby1.9 libs.
    Closes: #486487
  * Switched to using the extconf.rb class from ruby-pkg-tools. Will avoid
    future problems like this one.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 16 Jun 2008 14:57:18 +0200

libserialport-ruby (0.6-2) unstable; urgency=low

  * Bumped standards version to 3.7.3; no changes required.
  * Added Homepage and Vcs-* fields to debian/control.

 -- Paul van Tilburg <paulvt@debian.org>  Wed, 09 Apr 2008 09:01:11 +0200

libserialport-ruby (0.6-1) unstable; urgency=low

  * Initial release (Closes: #390418).

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Fri, 01 Dec 2006 20:59:07 +0100
